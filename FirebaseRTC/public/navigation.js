function opengit() {
    close("boardDIV");
    var element = document.getElementById("videoDIV");
    var otherelement = document.getElementById("gitDIV");
    if (otherelement.classList.contains("d-none")) {
        element.classList.remove("col-md-12");
        element.classList.add("col-md-6");
        otherelement.classList.remove("col-md-0");
        otherelement.classList.add("col-md-6");
        otherelement.classList.remove("d-none");
        otherelement.style.display = "block";
        console.log(element);
    } else {
        element.classList.remove("col-md-6");
        element.classList.add("col-md-12");
        otherelement.classList.remove("col-md-6");
        otherelement.classList.add("col-md-0");
        otherelement.classList.add("d-none");
    }
}

function openboard() {
    close("gitDIV");
    var element = document.getElementById("videoDIV");
    var otherelement = document.getElementById("boardDIV");
    if (otherelement.classList.contains("d-none")) {
        element.classList.remove("col-md-12");
        element.classList.add("col-md-6");
        otherelement.classList.remove("col-md-0");
        otherelement.classList.add("col-md-6");
        otherelement.classList.remove("d-none");
        otherelement.style.display = "block";
        console.log(element);
    } else {
        element.classList.remove("col-md-6");
        element.classList.add("col-md-12");
        otherelement.classList.remove("col-md-6");
        otherelement.classList.add("col-md-0");
        otherelement.classList.add("d-none");
    }
}

//--utiliy functions ---------

function close(s){
    var element = document.getElementById(s);
    element.classList.add("d-none");        
}
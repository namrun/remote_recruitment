import requests
import json


token = ''
sess_id = ''
sess_name = ''
companyId = '2461'
teamId = '2462'
userName = 'rem8@mail.com'
sub_id = ''

auth_url = 'https://cloudlabs.nuvepro.com/v1/users/login'
body = {'username': 'remoapiadmin@nuvelabs.com', 'password' : 'RemoAdmin@321'}
headers = {'Content-type': 'application/x-www-form-urlencoded'}

r = requests.post(auth_url, data=body, headers=headers)
p = (r.json())

#print(p["token"] , p["sessid"] , p["session_name"])

token = p["token"]
sess_id = p["sessid"]
sess_name = p["session_name"]
cook = {sess_name : sess_id}
#print('\n\n')
#------------------------------------------------------------------------------------------------------------------------------------#
def login():

    login_url = 'https://cloudlabs.nuvepro.com/v1/users'
    body = {'userName': userName, 'password' : 'Welcome123$', 'firstName' : 'remouser' , 'lastName': 'poc' , 'companyId' : '2461', 'teamId' : '2462'}
    header = {'Content-type': 'application/x-www-form-urlencoded', 'X-CSRF-Token' : token}

    w = requests.post(login_url, headers=header, data=body, cookies=cook)
    s = w.json()
    return(s)
#print('\n\n')

#--------------------------------------------------------------------------------------------------------------------------------------#
def add():

    add_users_url = 'https://cloudlabs.nuvepro.com/v1/users/addUserIntoTeams'
    header = {'Content-type': 'application/x-www-form-urlencoded', 'X-CSRF-Token' : token}
    add_body = {'userName' : userName , 'companyId' : '2461' , 'teamId' : '2462'}
    add_details = requests.post(add_users_url, headers=header, data=add_body, cookies=cook)

    return(add_details.json())

#-------------------------------------------------------------------------------------------------------------------------------------#

def createsub():
    sub_url = 'https://cloudlabs.nuvepro.com/v1/subscriptions'
    header = {'Content-type': 'application/x-www-form-urlencoded', 'X-CSRF-Token' : token}
    body = {'userName' : userName , 'planId' : '2466' , 'companyId' : '2461', 'teamId' : '2462'}
    create_sub = requests.post(sub_url, headers=header, data=body, cookies=cook)
    return(create_sub.json()['subscriptionId'])

#sub_id = createsub()

#-------------------------------------------------------------------------------------------------------------------------------------#

def launch():
    launch_url = 'https://cloudlabs.nuvepro.com/v1/subscriptions/launch'
    header = {'Content-type': 'application/x-www-form-urlencoded', 'X-CSRF-Token' : token}
    body = {'subscriptionId' : '115848'}
    launch_lab = requests.post(launch_url, headers=header, data=body, cookies=cook)
    return(json.loads(launch_lab.text))

p = launch()
p =(p['userAccess'])
print(json.loads(p)[1]['value'])
#print(launch())

const home = document.getElementById("home-section");
const about = document.getElementById("about-section");
const team = document.getElementById("team-section");
const contact = document.getElementById("contact-section");

function hide(item){
    item.classList.add("d-none");
    item.classList.remove("active");
}

function display(item){
    item.classList.remove("d-none")
}

function displayHome(){
    display(home);
    hide(about);
    hide(team);
    hide(contact);    
}

function displayAbout(){
    hide(home);
    display(about);
    hide(team);
    hide(contact);
}

function displayTeam(){
    hide(home);
    hide(about);
    display(team);
    hide(contact);
}

function displayContact(){
    hide(home);
    hide(about);
    hide(team);
    display(contact);
}